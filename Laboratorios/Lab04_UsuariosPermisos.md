---
title: "Usuarios y Permisos en Linux"
subtitle: "Virtualización: Laboratorio 4"
author: "Osmani Rosado-Falcón"
output: 
  html_document: 
    keep_md: yes
---

## Bibliografía

En el libro *Linux Pocket Guide* consultar:

- La sección *Users and Superusers* del capítulo *Linux: A First View*
- La sección *File Protections* del capítulo *The Filesystem*
- Los comandos `chmod`, `chown` y `chgrp` del capítulo *File Properties*
- Los comandos `useradd`, `userdel`, `usermod`, `passwd` y `chfn` del capítulo *User Account Management*
- Los comandos `groups`, `groupadd`, `groupdel` y `groupmod` del capítulo *Group Management*

Consultar el manual de usuario (`man`) del sistema para estudiar los comandos `adduser`, `sudo` y `su`.

## Ejercicios

1. En el directorio `/bin` están instalados un conjunto de programas disponibles a todos los usuarios del sistema. Por ejemplo, en Ubuntu el programa `bash` se encuentra en este directorio. Observe cuáles son los permisos de este programa. Use el comando `ls`.

2. El comando `sudo` permite a un usuario con los privilegios adecuados ejecutar un comando como el superusuario o como otro usuario. Use el comando `sudo touch` para crear un archivo vacio en el directorio `/root` (`home` del usuario root). Verifique que el archivo fue creado usando el comando `ls`.

3. Maria, Pedro y Julia tienen una tarea de programación en Perl que deben desarrollar en el sistema operativo GNU/Linux. Cree un usuario para cada uno de ellos en el sistema que usted administra. Use el comando `adduser` en lugar del comando `useradd`.
 
4. El comando `su` permite convertirse en otro usuario si conoce la contreseña de este. Use este comando para convertirse en el usuario Pedro. Ahora ejecute las operaciones siguientes a nombre de Pedro: 

    - obtener el directorio actual
    - crear un archivo en el *home* de Pedro
    - salir de la sección de Pedro usando el comando `exit`

5. Maria, Pedro y Julia desean compartir entre ellos algunos de sus archivos personales. Ellos necesitan un grupo para gestionar los permisos de los archivos que comparten. Cree un grupo con el nombre `amigos` y agrege a Maria, Pedro y Julia a este grupo. Use los comandos `groupadd` y `usermod`.

6. En el equipo de trabajo, Maria es la encargada de escribir el informe. Ella ha decidido escribir el informe en Latex. Maria debe crear los archivos `documento.tex` y `referencias.bib` en un directorio con el nombre `Informe`. Asuma el role de Maria y cree en el directorio `home` de Maria los archivos necesarios. Use los comandos `mkdir` y `touch`.

7. Maria necesita proteger su informe de los usuarios del sistema. Elimine todos los permisos otorgados a otros usuarios sobre el directorio `Informe` y los archivos que este contiene. Use el comando `chmod`.

8. Maria quiere compartir el informe que esta escribiendo con sus amigos. Ponga el grupo `amigos` al directorio `Informe` y a los archivos que este contiene. Use el comando `chgrp`. Los usuarios del grupo `amigos` no deben tener permisos de escritura sobre estos archivos. Si es necesario, modifique los permisos de estos archivos.

9. Julia tiene la tarea de revisar el informe que Maria esta escribiendo. Asuma el role de Julia y copie la versión actual del informe de Maria en el directorio *home* de Julia.

10. El programa `youtube-dl` descarga videos de YouTube. Para usarlo es necesario tener Python instalado. Instale el programa `youtube-dl` en el directorio `/usr/local/bin`, puede descargarlo usando este [enlace](https://youtube-dl.org/downloads/latest/youtube-dl). Los permisos del programa `youtube-dl` deben ser iguales a los permisos del programa `/bin/bash`. Use el comando `youtube-dl --help` para verificar si el programa funciona correctamente.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Virtualization Labs</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Osmani Rosado Falcón</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/osmanirosado/virtualization-labs" rel="dct:source">https://gitlab.com/osmanirosado/virtualization-labs</a>.

