---
title: "Uso de VirtualBox"
subtitle: "Virtualización: Laboratorio 1"
author: "Osmani Rosado-Falcón"
output: 
  html_document:
    keep_md: yes
---

## Bibliografía

- *Virtual Machines*, capítulo 18 del libro *Operating Systems Concepts Tenth Edition*
- [*Platform virtualization*](https://classroom.udacity.com/courses/ud189/lessons/655138541/concepts/6548285810923) del curso [*Introduction to Operating Systems*](https://classroom.udacity.com/courses/ud189)
- [*Red Hat Virtualization Tutorial*](https://www.redhat.com/es/topics/virtualization) (en español)
- [*Oracle Virtual Box User Manual*](https://www.virtualbox.org/manual/)

## Ejercicios

1. Instalar el VirtualBox. Consultar la [ayuda](#install-virtualbox).

2. Crear una máquina virtual para instalar Ubuntu Server 18.04. En la [guía](#new-vm) se explican las opciones que se deben escoger para crear la máquina.

3. Configurar dos interfaces de red para la máquina virtual. En la [guía](#conf-networks) se explica el tipo de interfaz a usar en cada caso.

4. Exportar para un archivo la máquina virtual. En la [guía](#export-vm) se muestran los pasos a seguir.

5. Importar una máquina virtual de Windows XP desde un archivo. En la [guía](#import-vm) se muestran los pasos a seguir.

6. Encender la máquina virtual de Windows XP, probar que funciona correctamente y apagarla luego. En la [ayuda](#start-stop-vm) se explican las opciones disponibles.

7. Instalar un programa en la máquina virtual de Windows XP, por ejemplo, el Git. Tomar una instantánea (*snapshot*) de la máquina virtual antes de instalar el programa y otra después de instalar el programa. Consultar la sección 1.11 [*Snapshots*](https://www.virtualbox.org/manual/ch01.html#snapshots) del manual de usuario de *Oracle Virtual Box*.

    Estas instantáneas se pueden ver como puntos de restauración. Por ejemplo, se pueden usar para retornar al estado anterior en los casos siguientes:

  - El sistema quedó en un estado no deseado después de la instación del programa: dejaron de funcionar correctamente otros programas. Una solución es restaurar el sistema al estado de la primera instantánea.
  - El usuario modificó la configuración del programa instalado, ahora no funciona como esperaba y no sabe cómo arreglarlo. Una solución es restaurar el sistema al estado de la segunda instantánea.

8. Crear y ejecutar una máquina virtual a partir de un disco virtual de [LuUbuntu](https://lubuntu.net/). Descargar el disco virtual de Lubuntu 18.04 disponible en la [página](https://www.osboxes.org/lubuntu/). La contrseña de la máquina virtual es el mismo nombre de usuario. Consultar la sección 3.7 [*Storage Settings*](https://www.virtualbox.org/manual/ch03.html#settings-storage) del manual de usuario de *Oracle Virtual Box*

    En [OS Boxes](https://www.osboxes.org) están disponibles los discos virtuales de otras distribuciones para VirtualBox y VMWare.

## Instalación de VirtualBox {#install-virtualbox}

El Virtual Box está disponible para los sistemas operativos: Windows, Ubuntu y MacOS X ([página de descarga](https://www.virtualbox.org/wiki/Downloads)). La versión 6.0.4 de Virtual Box puede ser instalada en arquitecturas de 64 bits. La instalación de Virtual Box en una arquitectura de 32 bits se debe efectuar usando la versión 5.2. Los detalles sobre la instalación de Virtual Box se pueden consultar en el capítulo 2, del manual de usuario.

## Creación de una Máquina Virtual {#new-vm}

En el capítulo 1 del manual de usuario, se puede consultar la sección *Creating Your First Virtual Machine*. A continuación se detallan los pasos a seguir para la creación de la máquina virtal.

1. Abrir el asistente para crear la máquina virtual (botón New).

![](images/Screenshot_New_01.png)

2. Indicar el nombre de la máquina virtual y el tipo de sistema operativo.

![](images/Screenshot_New_02.png)

Se debe escoger un nombre que permita identificar facilmente la máquina virtual. El tipo de sistema operativo debe ser `Linux` y la versión `Ubuntu (64-bit)`.

3. Seleccionar la cantidad de memoria permitida para la máquina virtual.

![](images/Screenshot_New_03.png)

En este caso, el tamaño recomendado por la aplicación es de 1024 MB de un total de 4 GB disponibles. Se debe seleccionar un valor que se encuentre dentro del rango señalado en verde, con el objetivo de no deteriorar el rendimiento del sistema operativo anfitrión (*Host OS*). Si se tiene memoria suficiente puede usarse 1024 MB, en caso contrario, seleccionar 512 MB.

4. Crear un disco virtual.

![](images/Screenshot_New_04.png)

Seleccionar la opción de crear un disco nuevo. Analice el resto de las opciones para que conozca las variantes disponibles.

5. Seleccionar el tipo de disco virtual.

![](images/Screenshot_New_05.png)

Seleccionar *VirtualBox Disk Image* (VDI).

6. Seleccionar el modo de asignación de espacio.

![](images/Screenshot_New_06.png)

El disco virtual (VDI) se creará como un archivo en un disco físico. El espacio para el disco virtual se puede reservar completamente en el momento de su creación (*Fixed size*) o se puede asignar de forma dinámica (*Dynamically allocated*). Seleccionar esta última opción.

7. Seleccionar el tamaño del disco y su ubicación en el sistema de archivos.

![](images/Screenshot_New_07.png)

Un tamaño máximo de 10GB es suficiente para instalar el sistema operativo y los programas necesarios. El disco virtual se debe ubicar en un disco físico con al menos 6GB de espacio libre, asumiendo que la asignación de espacio es dinámica.

8. Observar las características de la máquina virtual creada.

![](images/Screenshot_New_08.png)

Usando el botón *Settings* se puede acceder a una ventana de configuración donde se pueden cambiar las características de la máquina virtual.

## Configuración de las interfaces de red {#conf-networks}

En el capítulo 6, *Virtual Networking*, del manual de usuario, se puede obtener ayuda en las secciones:

- *Introduction to Networking Modes*
- *Bridged Networking*
- *Host-Only Networking*

A continuación se detallan los pasos a seguir para configurar las interfaces de red.

1. Seleccionar la máquina virtual y abrir la ventana de ajustes (botón Settings)

![](images/Network_01.png)

2. Seleccionar la pestaña Networks

![](images/Network_02.png)

3. En la opción *Attached to*, cambiar *NAT* por *Bridged Adapter*

![](images/Network_03.png)

4. Habilitar el adaptador 2 y seleccionar *Host-only Adapter* en la opción *Attached to*.

![](images/Network_04.png)

## Exportación de una máquina virtual {#export-vm}

Una máquina virtual puede ser exportada, creando una copia de su estado. Los pasos a seguir para exportar la máquina de Ubuntu, se detallan a continuación.

1. Abrir el asistente para exportar la máquina virtual (botón Export).

![](images/Screenshot_Export_01.png)

2. Seleccionar la máquina virtual a exportar

![](images/Screenshot_Export_02.png)

3. Expecificar el formato del archivo destino y su ubicación.

![](images/Screenshot_Export_03.png)

El archivo destino se debe ubicar en un disco con espacio suficiente. Si el nombre del archivo tiene la extensión `ova` se creará un archivo único. Si se usa la extension `ovf` se crearan varios archivos, entre ellos, uno con el contenido del disco virtual y otro con los ajustes o características de la máquina virtual. Use la extensión `ova` al nombrar el archivo.

4. Editar la descripción de la máquina virtual

![](images/Screenshot_Export_04.png)

En este paso se suministra información sobre la máquina virtual. En el ejemplo, se ha indicado la versión, la licencia y una breve descripción. En la práctica esta información es muy importante. Por ejemplo, en la discripción se pueden listar las aplicaciones instaladas.

## Importar una máquina virtual {#import-vm}

En el archivo `WindowsXP.ova` está guardada una máquina virtual de Windows XP. Los pasos a seguir para importar la máquina de Windows XP, se detallan a continuación.

1. Abrir el asistente para importar la máquina virtual (botón Import).

![](images/Screenshot_Import_01.png)

2. Seleccionar la máquina virtual a importar

![](images/Screenshot_Import_02.png)

3. Seleccionar la ubicación en disco de la máquina virtual

![](images/Screenshot_Import_03.png)

Seleccione un disco con espacio suficiente. Tenga en cuenta que la máquina virtual puede necesitar más espacio para su funcionamiento que el tamaño que ocupa cuando está exportada. Observe que se pueden cambiar varias características de la máquina virtual antes de ser importada.

## Encendido y apagado de la máquina virtual {#start-stop-vm}

Seleccionar e iniciar (botón Start) la máquina virtual de Windows XP.

![](images/Screenshot_Start.png)

La máquina virtual iniciada se muestra en la imagen siguiente. Explore el menú que brinda VirtualBox para que conozca las opciones disponibles.

![](images/Screenshot_Running.png)

Para apagar la máquina existen varias opciones. Una opción es indicar al sistema operativo que se apague.

![](images/Screenshot_Stop_01.png)

Otras opciones se pueden encontrar al abrir el cuadro de opciones disponible en el menú File botón Close.

![](images/Screenshot_Stop_02.png)

Existen 3 opciones. 

![](images/Screenshot_Stop_03.png)

Debe evitar la tercera opción: *Power off the machine* porque puede dañar el sistema oparativo (es como quitar la energía a una máquina real). 

La tercera opción: *Send the shutdown signal* tiene el mismo efecto de apagar mediante el sistema operativo. 

La primera opción es my útil: *Save the machine state* porque permite suspender su ejecución para reanudarla luego. De esta forma no se piede el estado de los procesos que se estaban ejecutando. Recuerde que algunos sistemas operativos no soportan la opción de hibernar.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Virtualization Labs</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Osmani Rosado Falcón</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/osmanirosado/virtualization-labs" rel="dct:source">https://gitlab.com/osmanirosado/virtualization-labs</a>.
