---
title: "Instalación y configuración de Ubuntu"
subtitle: "Virtualización: Laboratorio 2"
author: "Osmani Rosado-Falcón"
output:
  html_document:
    keep_md: yes
---

## Ejercicios

1. Instalar Ubuntu Server 18.04 en una máquina virtual. Usar la máquina virtual creada en el laboratorio 1. En el proceso de instalación, crear una partición para la raíz del sistema de archivos virtual `/` y otra partición para guardar los documentos de los usuarios del sistema `/home`. En la [guía](#ubun-install) se detallan los pasos a seguir.

2. Abrir una terminal en el sistema operativo anfitrión para ejecutar comandos en el sistema operativo invitado (Ubuntu). En esta terminal es posible copiar y pegar comandos, incluyendo cualquier texto. En la [ayuda](#ssh-client) se muestra como abrir una terminal remota tanto en Windows como en Ubuntu.

3. Abrir un explorador de archivos en el sistema operativo anfitrión para explorar los archivos del operativo invitado (Ubuntu). El explorador permite operaciones como: copiar o mover archivos entre los dos sistemas operativos y usar un editor en el sistema anfitrión para editar archivos del sistema invitado. En la [ayuda](#sftp-client) se muestra como abrir un explorador de archivos remoto tanto en Windows como en Ubuntu.

4. Configurar el manejador de paquetes de Ubuntu para que use los repositorios de Ubuntu que mantiene la universidad. Sequir la instrucciones de la [guía](#ubun-repos). Antes de realizar este ejercicio, cree un *snapshot* de la máquina virtual por si ocurre algún problema y necesita regresar al estado actual.

5. Instalar el programa `tree`. Usar este programa para crear un árbol del directorio `/etc/apt`. En la [guía](#install-tree) se muestran los pasos que debe seguir.

6. Usar el comando `tree` para crear un árbol del sistema de archivos virtual montado en `/`. Dirigir la salida estándar del comando al archivo `root-tree.txt` y la salida de error al archivo `root-tree-errors.txt`. Revisar el archivo de errores.

7. Un usuario necesita que configure los repositorios de acuerdo a los pasos siguientes: primero deshabilitar los fuentes originales y luego habilitar los fuentes de la universidad. Para deshabilitar los fuentes originales se deben comentar las líneas que comienzan con `deb`, en el archivo `/etc/apt/sources.list`, usando el carater `#`. Para habilitar los fuentes de la universidad se deben copiar al final del archivo `/etc/apt/sources.list`. De esta forma todos los fuentes quedan en un mismo archivo. Para activar un fuente basta con descomentar una línea que comience con `# deb`.
    
    Escribir un programa en `perl` que comente los fuentes. Use las facilidades que brinda `perl`, específicamente el operador `s///`, para el trabajo con expresiones regulares. Para facilitar el trabajo, copie el archivo original `/etc/apt/sources.list.bak` en su directorio *home*.

8. Crear una nueva versión del programa orientado en el ejercicio anterior. En este caso, el programa debe hacer lo contrario: descomentar las líneas que empiezan con `# deb`.

9. Escribir un programa en Bash para automatizar la configuración de los repositorios de la universidad. El programa debe deshabilitar los fuentes originales, habilitar los fuentes de la universidad y actualizar la base de datos de APT.
  
## Instalación de Ubuntu {#ubun-install}

En la [guía](https://tutorials.ubuntu.com/tutorial/tutorial-install-ubuntu-server#0) de instalación de Ubuntu Server 18.04, se detallan los pasos que se deben seguir. En esta guía se han cambiado algunos pasos para cumplir con los requisitos del ejercicio. Usar el ISO de Ubuntu Server 18.04  disponible en este [enlace](http://old-releases.ubuntu.com/releases/18.04.0/) con el nombre `ubuntu-18.04-live-server-amd64.iso`.

1. Seleccione el idioma inglés

![](images/Installation_01.png)

2. Seleccionar la disposición del teclado

![](images/Installation_02.png)

3. Seleccionar la opción *Install Ubuntu*

![](images/Installation_03.png)

4. Configurar las conexiones de red

![](images/Installation_04.png)

En esta pantalla deben aparecer dos interfaces de red, si se creó la máquina virtual de acuerdo al laboratorio 1. 

Una de las interfaces de red debe tener un IP de la red 192.168.56.0/24, por ejemplo 192.168.56.101. En esta red la máquina anfitrion tiene el IP 192.168.56.1. La configuración de esta red virtual se puede revisar usando el Virtual Box.

La otra interfaz de red debe tener un IP en la red a la que está conectado el host. Por ejemplo, si el host está conectado a la red de la Facultad y tiene IP 10.12.4.80, la máquina virtual puede tener el IP 10.12.4.56; asumiendo que en dicha red hay un servidor DHCP.

Cuando no este conectado a una red, puede usar la red virtual para la comunicación entre el sistema anfitrión y la máquina virtual.

5. No configurar el proxy

![](images/Installation_05.png)

No tiene sentido configurar el proxy en este paso por varias razones:

- La mayoría de las aplicaciones no soportan el tipo de autenticación *digest* del proxy de la universidad.
- Luego se configura la máquina virtual para usar los repositorios de Ubuntu de la universidad.
- El proxy se puede configurar posteriormente para algunos programas que si soportan la autenticación *digest*, como es el caso del comando `git`.

6. Preparar el sistema de archivos

![](images/Installation_06.png)

El disco virtual está vacio y se puede usar completamente. Seleccionar la opción *Use An Entire Disk*.

7. Seleccionar el disco para la instalación

![](images/Installation_07.png)

En el laboratorio 1 se creó este disco.

8. Editar las particiones

![](images/Installation_08.png)

Seleccionar la opción *Edit Partitions* para crear dos particiones, una para el sistema `/` y la otra para el `/home` de los usuarios.

9. Seleccionar la partición 2 para edición

![](images/Installation_09.png)

10. Cambiar el tamaño de la partición

![](images/Installation_10.png)

Cambiar el tamaño de la partición a 7.4 GiB, dejar `ext4` como sistema de archivos y `/` como punto de montaje.

11. Agregar una partición en el espacio libre

![](images/Installation_11.png)

12. Editar la nueva partición

![](images/Installation_12.png)

Usar todo el espacio libre para crear la nueva partición, mantener el sistema de archivos `ext4` y 
seleccionar `/home` como punto de montaje.

13. Verificar y guardar los cambios

![](images/Installation_13.png)

![](images/Installation_13b.png)

14. Confirmar la creación de las particiones

![](images/Installation_14.png)

En este caso no existe peligro de pérdida de datos, pero en un entorno de producción, se debe revisar bien antes de continuar.

15. Entrar la información de la máquina

![](images/Installation_15.png)

Escribir el nombre completo (*Your name*) y seleccionar un nombre de usuario (*Pick a username*), puede usar el mismo nombre de usuario que tiene en la universidad. Escoger una contraseña segura para la máquina, recuerde que la máquina se conecta a la red. En el campo *Your server's name* puede poner el nombre que desee. 

16. Reiniciar cuando termine la instalación

![](images/Installation_16.png)

17. Entrar al sistema usando el nombre de usuario configurado durante el proceso de instalación. 

    El sistema operativo se puede apagar usando el comando `poweroff`, o el comando `reboot` si desea iniciarlo nuevamente.

## Cliente SSH {#ssh-client}

Para abrir una terminal remota se puede usar un cliente SSH (*Secure Shell*).

### Ubuntu

El comando siguiente permite abrir el shell del usuario `osmani` en la en la máquina que tiene el ip `192.168.56.104`. En la imagen se puede observar como se ha entrado al sistema remoto después de escribir la contraseña del usuario. Observe que ha cambiado el *prompt*.


```bash
ssh osmani@192.168.56.104
```

![](images/Bash_01.png)

### Windows

En Windows 10 se puede usar de forma similar el comando `ssh` si se dispone del *shell* `PowerShell`. La imagen siguiente muestra una terminal en Ubuntu donde primero se ha abierto el `powershell` y luego se ha ejecutado el comando `ssh`. En Windows se debe obtener una salida similar.

![](images/PowerShell_01.png)

### PuTTY

En las versiones de Windows que no dispongan de `PowerShell` se puede usar el programa [PuTTY](https://www.putty.org/).

1. Abrir el Putty

![](images/Putty_01.png)

Poner el IP de la máquina virtual y presionar *Open*.

2. Acceptar la llave pública de la máquina virtual.

![](images/Putty_02.png)

Esta llave se usa para establecer conecciones seguras.

3. Entrar el usuario y la contraseña

![](images/Putty_03.png)

## Cliente SFTP {#sftp-client}

Para abrir un explorador de archivos remoto se puede usar un cliente SFTP (*Secure* FTP).

## Ubuntu

En Ubuntu el explorador de archivos soporta el protocolo SFTP.

1. En la barra de dirección insertar `sftp://192.168.56.104` y presionar *Enter*.

![](images/Dolphin_01.png)

2. Acceptar la llave pública de la máquina virtual.

![](images/Dolphin_02.png)

3. Insertar el usuario y la contraseña

![](images/Dolphin_03.png)

4. Usar el explorador de archivos remoto

![](images/Dolphin_04.png)

En la imagen siguiente se muestra el sistema de archivos local y el remoto.

![](images/Dolphin_05.png)

En el panel de la derecha se muestra el sistema de archivos local y en el panel de la izquierda se muestra el sistema de archivos remoto. Si no puede abrir dos paneles en el mismo explorador, use dos instancias del explorador.

## Windows

En Windows para acceder al sistema de archivos remoto se puede usar el programa [WinSCP](https://winscp.net/).

1. Ingresar el IP de la máquina, el usuario y la contraseña.

![](images/WinSCP_01.png)

2. Acceptar la llave pública de la máquina virtual.

![](images/WinSCP_02.png)

3. Usar el explorador de archivos remoto

![](images/WinSCP_03.png)

En el panel de la derecha se puede acceder al sistema de archivos local y en el panel de la izquierda puede acceder al sistema de archivos remoto.

## Configuración de los repositorios {#ubun-repos}

1. Deshabilitar los repositorios oficiales de Ubuntu para usar los repositorios que mantiene la universidad.

Renombrar el archivo `/etc/apt/sources.list`


```bash
sudo mv /etc/apt/sources.list /etc/apt/sources.list.bak
```

2. Descargar la configuración del repositorio de la universidad


```bash
curl -o sources.list "http://redtic.uclv.edu.cu/dokuwiki/_export/code/repo-ubuntu?codeblock=0"
```

3. Copiar el archivo descargado en la dirección `/etc/apt/sources.list`


```bash
sudo cp sources.list /etc/apt/sources.list
```

4. Descargar la información de los paquetes para los fuentes configurados


```bash
sudo apt update
```

5. Actualizar los paquetes instalados


```bash
sudo apt upgrade
```

6. Limpiar los paquetes no necesarios que pudieron quedar después de la actualización del sistema. 


```bash
sudo apt autoremove
```

7. Reiniciar el sistema operativo para probar que inicia correctamente después de la actualización.


```bash
reboot
```

## Instalación del comando `tree` {#install-tree}

Para instalar paquetes o programas desde el repositorio se puede usar el comando `apt`. En el ejemplo siguiente se usa la opción `install` de `apt` para instalar el programa `tree`.


```bash
sudo apt install tree
```

El comando siguiente imprime un árbol que muestra el contenido del directorio `/etc/apt`


```bash
tree /etc/apt
```
```
## /etc/apt
## ├── apt.conf.d
## │   ├── 01autoremove
## │   ├── 01autoremove-kernels
## │   ├── 01-vendor-ubuntu
## │   ├── 10periodic
## │   ├── 15update-stamp
## │   ├── 20archive
## │   ├── 20auto-upgrades
## │   ├── 20snapd.conf
## │   ├── 50command-not-found
## │   ├── 50unattended-upgrades
## │   ├── 70debconf
## │   ├── 90cloud-init-pipelining
## │   └── 99update-notifier
## ├── preferences.d
## ├── sources.list
## ├── sources.list.bak
## ├── sources.list.d
## └── trusted.gpg.d
##     ├── ubuntu-keyring-2012-archive.gpg
##     ├── ubuntu-keyring-2012-cdimage.gpg
##     └── ubuntu-keyring-2018-archive.gpg
## 
## 4 directories, 18 files
```

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Virtualization Labs</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Osmani Rosado Falcón</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/osmanirosado/virtualization-labs" rel="dct:source">https://gitlab.com/osmanirosado/virtualization-labs</a>.
