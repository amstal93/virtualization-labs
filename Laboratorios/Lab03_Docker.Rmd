---
title: "Primeros pasos con Docker"
subtitle: "Virtualización: Laboratorio 3"
author: "Osmani Rosado-Falcón"
output: 
  html_document: 
    keep_md: yes
---

## Bibiografía

- Documentación Oficial de Docker ([sitio](https://docs.docker.com/))
    - [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
    - [Get started with Docker](https://docs.docker.com/get-started/)
    - [docker CLI Reference](https://docs.docker.com/engine/reference/commandline/docker/)
- [The Docker Book](https://dockerbook.com/)

### Video tutoriales

- [What is a Container?](https://www.youtube.com/watch?v=EnJ7qX9fkcU)
- [Why containers?](https://www.youtube.com/watch?v=n-JwAM6XF88)
- [Containers and VMs - A Practical Comparison](https://www.youtube.com/watch?v=L1ie8negCjc)

## Ejercicios

1. Instalar Docker usando los repositorios de la universidad. Seguir la [guía](#install-docker).

2. Configurar Docker para que use el registro de Docker que mantiene la universidad. El objetivo es que Docker use este registro para descargar las imágenes. Seguir la [guía](#config-registry).

3. Visitar la página de la imágen oficial de Bash ([enlace](https://hub.docker.com/_/bash)) en [Docker Hub](https://hub.docker.com/). Cada imágen de Bash está estiquetada usando la versión de Bash. Por ejemplo, la versión de Bash 5.0 está disponible en la imágen `bash:5.0`. ¿Qué otras versiones están disponibles?

4. Iniciar un contenedor a partir de la imágen `bash:5.0`. Use el comando `docker run` con las opciones -i y -t para obtener el intérprete de Bash en una terminal. Estudie estas opciones en el manual de Docker. Puede obtener ayuda use el comando `docker run --help`. Ejecute los siguientes comandos en el contenedor:

    - Use el comando `env` para obtener las variables de ambiente. ¿Cuáles son los valores de las variables de ambiente `HOSTNAME`, `HOME`, `PATH`?
    - Use el comando `echo` para imprimir el valor de la variable de ambiente `PATH`.
    - Use el comando `pwd` para obtener el directorio actual.
    - Use el comando `exit` para salir de intérprete de Bash. Note que también se detiene el contenedor.  

5. Listar los contenedores usando el comando `docker ps` con la opción `-a`. Estudie la opción `-a`. En la lista de los contenedores debe aparecer el contenedor de Bash ejecuta anteriormente. Recuerde que el comando `docker run` crea un contenedor y luego lo inicia. Analice la información obtenida sobre el contenedor. ¿Cuál es el identificador del contenedor? ¿Qué imágen se usó para crearlo?

6. Eliminar el contenedor de Bash creado anteriormente usando el comando `docker rm`. Use el identificador del contenedor en esta operación. Verifique que el contenedor fue eliminado.

7. Iniciar un contenedor de Bash. Use la opción `-e` del comando `docker run` para indicar el valor de la variable de ambiente `HOSTNAME`. Use un comando dentro del contenedor para mostrar el valor de esta variable.

8. Descargar la imágen de Bash 4.4 usando el comando `docker pull`. Liste las imágenes locales usando el comando `docker images`. Analice la información obtenida sobre la imágen descargada. Observe cómo se descompone el nombre de la imágen en repositorio y etiqueta. ¿Qué tamaño tiene la imágen?

9. Estudiar el ejemplo hello de la conferencia. En este ejemplo se usó la imágen de Ubuntu 18.04 como base. Modifique el Dockerfile para usar la imágen de Bash 4.4 en lugar de la imágen de Ubuntu. Estudie la sentencia `FROM`. 

    - Construir la imágen usando el comando `docker build` con la opción `-t`. Use la opción `-t` para ponerle el nombre `ejemplo-hello` a la imágen. 

    - Pruebe la imágen usando el comando `docker run`. Recuerde asignarle un valor a la variable de ambiente `NAME`.
    
    El código del ejemplo hello se muestra a continuación.

    ```[bash]
    cat hello.sh
    ```
    ```
    #!/usr/bin/env bash
    
    echo Hello $NAME
    ```
    
    ```[bash]
    cat Dockerfile
    ```
    ```
    FROM ubuntu:bionic
    
    WORKDIR /home
    
    COPY hello.sh .
    RUN chmod u+x hello.sh
    
    CMD ./hello.sh
    ```

10. Mejorar el ejemplo hello de la conferencia. El programa debe escribir en la terminal "Hello Word!" en lugar de "Hello", si la variable de ambiente `NAME` no está definida. Pruebe el programa antes de construir la imágen. Use la ayuda siguiente:

    - Ejecute el programa sin definir la variable de ambiente NAME para verificar que escribe "Hello Word!".
    - Defina la variable de ambiente NAME usando el comando `export NAME="Maria"`. Ejecute el programa para verificar que escribe "Hello Maria!".

11. Construir una imágen con el Java Development Kit. Use como base la imágen de Ubuntu 18.04 (bionic) e instale el Open JDK 8 de Java. El paquete `openjdk-8-jdk` permite instalar el JDK 8 desde los repositorios de Ubuntu. Use el comando `apt-get` para instalar este paquete.

12. Probar la imágen creada con el JDK 8. Use la opción `-v` del comando `docker run` para montar un directorio (ej: `/home/julia/ejemplo-java`) de su máquina virtual dentro del contenedor. Escriba un programa en Java y guardelo dentro de ese directorio. 

    - Edite el programa en la máquina virtual.
    - Use el comando `javac Main.java` para compilar el programa dentro del contenedor.
    - Asumiendo que en el directorio actual se encuentra el archivo `Main.class`, use el comando `java -classpath . Main` para ejecutar el programa dentro del contenedor. 

13. Hacer una lista de algunas imágenes de Docker. Incluya las imágenes de los lenguajes siquientes:

    - Python
    - Perl
    - Scala
    - Haskell
    
    Estas imágenes pueden usarse para ejecutar programas escritos en estos lenguajes.

## Instalación de Docker {#install-docker}

1. Crear un archivo con el nombre `docker.list` para guardar la línea siguiente. En esta línea se especifica el repositorio de Docker que mantiene la universidad.

    ```
    deb http://repos.uclv.edu.cu/docker-ce/linux/ubuntu bionic stable
    ```

2. Copiar el archivo `docker.list` en el directorio `/etc/apt/sources.list.d`

    ```{bash eval=FALSE}
    sudo cp docker.list /etc/apt/sources.list.d
    ```

3. Agregar la llave pública del repositorio

    Usar el comando `curl` para descargar la llave pública del repositorio de Docker de la universidad. El comando siguienete descarga la llave y la guarda en el archivo `docker.key`.

    ```{bash eval=FALSE}
    curl -o docker.key http://repos.uclv.edu.cu/docker-ce/linux/ubuntu/gpg
    ```

    Usar el comando `apt-key` para agregar la llave.

    ```{bash eval=FALSE}
    sudo apt-key add docker.key
    ```

4. Actualizar la información de los fuentes

    ```{bash eval=FALSE}
    sudo apt update
    ```

5. Instalar el Docker

    ```{bash eval=FALSE}
    sudo apt install docker-ce
    ```

6. Agregar el usuario actual (`$USER`) al grupo `docker` para que tenga permiso de ejecutar el comando `docker`, de lo contrario tendrá que usar siempre el comando `sudo` para ejecutar el comado de `docker`.

    ```{bash eval=FALSE}
    sudo usermod -G docker -a $USER
    ```

7. Reiniciar el entorno del usuario actual como si hubiera iniciado sección, para que el *shell* conozca que el usuario pertenece ahora al grupo `docker`.

    ```{bash eval=FALSE}
    su - $USER
    ```

    Para obtener ayuda consultar la [pregunta](https://superuser.com/questions/272061/reload-a-linux-users-group-assignments-without-logging-out).


8. Usar el comando siquiente para probar la instalación

    ```{bash eval=FALSE}
    docker info
    ```

La guía oficial para la instalación de Docker mantenida por la universidad se encuentra en: [página](http://redtic.uclv.edu.cu/dokuwiki/dockerinstall).

## Configurar un registro de Docker {#config-registry}

1. Crear un archivo de configuración para Docker en `/etc/default`, con la variable de ambiente `DOCKER_OPTS`. Esto se puede hacer usando el comando siguiente. En este caso se configuran los DNS y el registro de Docker de la universidad.

    ```{bash eval=FALSE}
    sudo tee /etc/default/docker <<'EOF'
    DOCKER_OPTS="--registry-mirror=https://nexus.uclv.edu.cu"
    EOF
    ```

    La entrada estándar del comando anterior se toma de un archivo que ha sido definido directamente después del comando. Estudiar luego la sección [*Bash Document Here*](https://www.gnu.org/software/bash/manual/html_node/Redirections.html#Here-Documents) del manual de Bash.

2. Configurar el servicio de Docker haciendo los cambios siguientes en el archivo `/lib/systemd/system/docker.service`.

    a) Cambiar el valor de la variable `ExecStart`. Si el valor actual es:

    ```
    ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
    ```

    Su nuevo valor debe ser:

    ```
    ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock $DOCKER_OPTS
    ```

    b) En una línea anterior a la definición de la variable `ExecStart` definir la varible `EnvironmentFile` para que la variable de ambiente `DOCKER_OPTS` sea cargada y se use el registro de la universidad para obtener las imágenes.

    ```
    EnvironmentFile=/etc/default/docker
    ```

3. Recargar el manejador de configuraciones de `systemd` para cargar los cambios hechos en la configuración del servicio (*daemon*) de Docker.

    ```{bash eval=FALSE}
    sudo systemctl daemon-reload
    ```

4. Reiniciar el servicio de Docker

    ```{bash eval=FALSE}
    sudo service docker restart
    ```

5. Descargar la imágen `hello-world` para comprobar si el servicio de Docker usa correctamente el registro de la universidad. Se asume que el servicio de Docker no tiene acceso directo a internet.

    ```{bash eval=FALSE}
    docker pull hello-world
    ```
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Virtualization Labs</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Osmani Rosado Falcón</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/osmanirosado/virtualization-labs" rel="dct:source">https://gitlab.com/osmanirosado/virtualization-labs</a>.

