import django
from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import redirect


from .models import Touch


def index(request):
    context = {'count': Touch.objects.count(), 'last': Touch.objects.last()}
    return render(request, 'touchs/index.html', context)


def touch(request):
    o = Touch()
    o.timestamp = django.utils.timezone.now()
    o.save()

    return redirect('index')
